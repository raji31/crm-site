from flask import Flask, request,render_template
from crm import crm_routes
import os 
import json


app = Flask(__name__, instance_relative_config=False)
app.register_blueprint(crm_routes.crm_bp)
app.config.from_json("config.json")

@app.errorhandler(404)
def get_404(err):
    return """<em> 404: You have reached a wrong part of the site. 
              Click <a href='https://www.raj302.com' >here<a> to return back to the main site. </em>""", 404
    
application=app

if __name__=="__main__":
    application.run(port=5000,debug=True)